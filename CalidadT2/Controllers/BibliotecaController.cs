﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;
using CalidadT2.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CalidadT2.Repositorios;
using CalidadT2.Services;

namespace CalidadT2.Controllers
{
    [Authorize]
    public class BibliotecaController : Controller
    {
        private readonly IBibliotecaRepositorio  biblioteca;
        private readonly IAuthService auth;
        private readonly IUsuarioRepository usuario;


        public BibliotecaController(IBibliotecaRepositorio biblioteca, IAuthService auth, IUsuarioRepository usuario)
        {
            this.biblioteca = biblioteca;
            this.auth = auth;
            this.usuario = usuario;
        }

        [HttpGet]
        public IActionResult Index()
        {
            Usuario user = LoggedUser();
            var model = biblioteca.GetAll(user);
            return View(model);
        }

        [HttpGet]
        public ActionResult Add(int libro)
        {
            Usuario user = LoggedUser();
            biblioteca.Add(libro, user);            
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult MarcarComoLeyendo(int libroId)
        {
            Usuario user = LoggedUser();

            biblioteca.MarcarComoLeyendo(libroId, user);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult MarcarComoTerminado(int libroId)
        {
            Usuario user =LoggedUser();

            biblioteca.MarcarComoTerminado(libroId, user);
            return RedirectToAction("Index");
        }

        private Usuario LoggedUser()
        {
            auth.SetHttpContext(HttpContext);
            var claim = auth.ObtenerClaim();
            var user = usuario.ObtenerUsuarioLoggin(claim);
            return user;
        }
    }
}
