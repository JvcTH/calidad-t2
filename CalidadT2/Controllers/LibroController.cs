﻿using System;
using System.Linq;
using CalidadT2.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using CalidadT2.Repositorios;
using CalidadT2.Services;

namespace CalidadT2.Controllers
{
    public class LibroController : Controller
    {
        private readonly IUsuarioRepository usuario;
        private readonly IAuthService auth;
        private readonly ILibroRepositorio libro;
        private readonly IComentarioRepositorio rComentario;

        public LibroController(IUsuarioRepository usuario, IAuthService auth, ILibroRepositorio libro, IComentarioRepositorio rComentario)
        {
            this.usuario = usuario;
            this.auth = auth;
            this.libro = libro;
            this.rComentario = rComentario;
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            var model = libro.DetalleLibro(id);
            return View(model);
        }

        [HttpPost]
        public IActionResult AddComentario(Comentario comentario)
        {
            Usuario user = LoggedUser();
            rComentario.AddComentario(user, comentario);
            return RedirectToAction("Details", new { id = comentario.LibroId });
        }

        private Usuario LoggedUser()
        {
            auth.SetHttpContext(HttpContext);
            var claim = auth.ObtenerClaim();
            var user = usuario.ObtenerUsuarioLoggin(claim);
            return user;
        }
    }
}
