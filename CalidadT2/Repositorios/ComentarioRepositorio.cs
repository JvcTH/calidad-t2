﻿using CalidadT2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    public interface IComentarioRepositorio
    {
        public void AddComentario(Usuario user, Comentario comentario);
    }
    public class ComentarioRepositorio
    {
        private AppBibliotecaContext app;

        public ComentarioRepositorio(AppBibliotecaContext context)
        {
            app = context;
        }

        public void AddComentario(Usuario user, Comentario comentario)
        {
            comentario.UsuarioId = user.Id;
            comentario.Fecha = DateTime.Now;
            app.Comentarios.Add(comentario);

            var libro = app.Libros.Where(o => o.Id == comentario.LibroId).FirstOrDefault();
            libro.Puntaje = (libro.Puntaje + comentario.Puntaje) / 2;

            app.SaveChanges();
        }
    }
}
