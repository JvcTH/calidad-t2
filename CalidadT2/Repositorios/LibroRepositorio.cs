﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CalidadT2.Repositorios
{
    public interface ILibroRepositorio
    {
        public List<Libro> Libros();
        public Libro DetalleLibro(int id);
    }

    public class LibroRepositorio : ILibroRepositorio
    {
        private AppBibliotecaContext app;

        public LibroRepositorio(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public List<Libro> Libros()
        {
            var model = app.Libros.Include(o => o.Autor).ToList();
            return model;
        }

        public Libro DetalleLibro(int id)
        {
            var model = app.Libros
                .Include("Autor")
                .Include("Comentarios.Usuario")
                .FirstOrDefault(o => o.Id == id);
            return model;
        }
    }
}
