﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Models;
using CalidadT2.Constantes;
using System.Security.Claims;

namespace CalidadT2.Repositorios
{
    public interface IUsuarioRepository
    {
        Usuario FindUserByCredentials(string username, string password);
        public Usuario ObtenerUsuarioLoggin(Claim claim);
    }
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppBibliotecaContext context;

        public UsuarioRepository(AppBibliotecaContext context)
        {
            this.context = context;
        }
        public Usuario FindUserByCredentials(string username, string password)
        {
            return context.Usuarios
               .FirstOrDefault(o => o.Username == username && o.Password == password);
        }

        public Usuario ObtenerUsuarioLoggin(Claim claim)
        {
            var user = context.Usuarios.FirstOrDefault(o => o.Username == claim.Value);
            return user;
        }
    }
}
