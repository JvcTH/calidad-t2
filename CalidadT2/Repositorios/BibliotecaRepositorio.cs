﻿using CalidadT2.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CalidadT2.Constantes;

namespace CalidadT2.Repositorios
{

    public interface IBibliotecaRepositorio
    {
        List<Biblioteca> GetAll(Usuario user);
        void Add(int libro, Usuario user);
        void MarcarComoLeyendo(int libroId, Usuario user);
        void MarcarComoTerminado(int libroId, Usuario user);

    }
    public class BibliotecaRepositorio : IBibliotecaRepositorio
    {
        private readonly AppBibliotecaContext app;

        public BibliotecaRepositorio(AppBibliotecaContext app)
        {
            this.app = app;
        }

        public void Add(int libro, Usuario user)
        {
            var biblioteca = new Biblioteca
            {
                LibroId = libro,
                UsuarioId = user.Id,
                Estado = ESTADO.POR_LEER
            };
            app.Bibliotecas.Add(biblioteca);
            app.SaveChanges();

        }

        public List<Biblioteca> GetAll(Usuario user)
        {
           return app.Bibliotecas
                .Include(o => o.Libro.Autor)
                .Include(o => o.Usuario)
                .Where(o => o.UsuarioId == user.Id)
                .ToList();
        }

        public void MarcarComoLeyendo(int libroId, Usuario user)
        {
            var libro = app.Bibliotecas
               .Where(o => o.LibroId == libroId && o.UsuarioId == user.Id)
               .FirstOrDefault();

            libro.Estado = ESTADO.LEYENDO;
            app.SaveChanges();
        }

        public void MarcarComoTerminado(int libroId, Usuario user)
        {
            var libro = app.Bibliotecas
               .Where(o => o.LibroId == libroId && o.UsuarioId == user.Id)
               .FirstOrDefault();

            libro.Estado = ESTADO.TERMINADO;
            app.SaveChanges();
        }
    }
}
