﻿using System;
using System.Collections.Generic;
using System.Text;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Pruebas
{
    class AuthControllerTest
    {
        public void informacionUsuario(Usuario usuario)
        {
            usuario.Nombres = "Jose";
            usuario.Password = "admin";
            usuario.Username = "admin";
        }
        [Test]
        public void UsuarioJuanSeLoguea()
        {
            var usuario = new Usuario();
            informacionUsuario(usuario);
            var userMock = new Mock<IUsuarioRepository>();
            userMock.Setup(o => o.FindUserByCredentials(usuario.Username, usuario.Password)).Returns(usuario);
            var cookMock = new Mock<IAuthService>();
            var authCont = new AuthController(userMock.Object, cookMock.Object);
            var log = authCont.Login("admin", "admin");
            Assert.IsInstanceOf<RedirectToActionResult>(log);
        }


    }
}
