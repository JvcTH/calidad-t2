﻿using System;
using System.Collections.Generic;
using System.Text;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using CalidadT2.Services;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
namespace Pruebas
{
    class LibroControllerTest
    {
        public void informacionUsuario(Usuario usuario)
        {
            usuario.Nombres = "Jose";
            usuario.Password = "admin";
            usuario.Username = "admin";
        }
        [Test]
        public void EntraDetalleDeUnLibro()
        {
            var usuario = new Usuario();
            informacionUsuario(usuario);
            var libromoq = new Mock<ILibroRepositorio>();
            libromoq.Setup(o => o.DetalleLibro(3)).Returns(new Libro());

            var detalleLibroController = new LibroController(null, null, libromoq.Object, null);
            var guardarCom = detalleLibroController.Details(3);

            Assert.IsInstanceOf<ViewResult>(guardarCom);
        }
        [Test]
        public void AgregaComentarioLibro()
        {
            var usuario = new Usuario();
            informacionUsuario(usuario);
            var userMock = new Mock<IUsuarioRepository>();
            userMock.Setup(o => o.ObtenerUsuarioLoggin(null)).Returns(usuario);
            var comentarioMoq = new Mock<IComentarioRepositorio>();
            comentarioMoq.Setup(o => o.AddComentario(usuario, new Comentario()));
            var cookMock = new Mock<IAuthService>();
            var ComentarioLibroController = new LibroController(userMock.Object, cookMock.Object, null, comentarioMoq.Object);
            var guardarCom = ComentarioLibroController.AddComentario(new Comentario());
            Assert.IsInstanceOf<RedirectToActionResult>(guardarCom);
        }
    }
}
