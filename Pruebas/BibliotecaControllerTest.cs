﻿using System;
using System.Collections.Generic;
using System.Text;
using CalidadT2.Controllers;
using CalidadT2.Models;
using CalidadT2.Repositorios;
using CalidadT2.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace Pruebas
{
    class BibliotecaControllerTest
    {
        public void informacionUsuario(Usuario usuario)
        {
            usuario.Nombres = "Jose";
            usuario.Password = "admin";
            usuario.Username = "admin";
        }
        [Test]
        public void UsuarioEntraAlIndex()
        {
            var userMock = new Mock<IUsuarioRepository>();
            userMock.Setup(o => o.ObtenerUsuarioLoggin(null)).Returns(new Usuario() { });
            var bibliMoq = new Mock<IBibliotecaRepositorio>();
            bibliMoq.Setup(o => o.GetAll(new Usuario()));
            var cookMock = new Mock<IAuthService>();
            var indexBibliotecaController = new BibliotecaController(bibliMoq.Object, cookMock.Object, userMock.Object);
            var guardarCom = indexBibliotecaController.Index();
            Assert.IsInstanceOf<ViewResult>(guardarCom);
        }

        [Test]
        public void AgregaLibroAlaBiblioteca()
        {
            var usuario = new Usuario();
            informacionUsuario(usuario);
            var userMock = new Mock<IUsuarioRepository>();
            userMock.Setup(o => o.ObtenerUsuarioLoggin(null)).Returns(new Usuario() { });
            var bibliMoq = new Mock<IBibliotecaRepositorio>();
            bibliMoq.Setup(o => o.GetAll(new Usuario()));
            var cookMock = new Mock<IAuthService>();
            var AgregarLibroBibliotecaController = new BibliotecaController(bibliMoq.Object, cookMock.Object, userMock.Object);
            var guardarCom = AgregarLibroBibliotecaController.Add(3);
            Assert.IsInstanceOf<RedirectToActionResult>(guardarCom);
        }

        [Test]
        public void MarcaComoLeidoUnLibro()
        {
            var usuario = new Usuario();
            informacionUsuario(usuario);
            var userMock = new Mock<IUsuarioRepository>();
            userMock.Setup(o => o.ObtenerUsuarioLoggin(null)).Returns(new Usuario() { });
            var bibliMoq = new Mock<IBibliotecaRepositorio>();
            bibliMoq.Setup(o => o.GetAll(new Usuario()));
            var cookMock = new Mock<IAuthService>();
            var MarcarLeidoBibliotecaController = new BibliotecaController(bibliMoq.Object, cookMock.Object, userMock.Object);
            var guardarCom = MarcarLeidoBibliotecaController.MarcarComoLeyendo(3);
            Assert.IsInstanceOf<RedirectToActionResult>(guardarCom);
        }

        [Test]
        public void MarcaComoTerminadoUnLibro()
        {
            var usuario = new Usuario();
            informacionUsuario(usuario);
            var userMock = new Mock<IUsuarioRepository>();
            userMock.Setup(o => o.ObtenerUsuarioLoggin(null)).Returns(new Usuario() { });
            var bibliMoq = new Mock<IBibliotecaRepositorio>();
            bibliMoq.Setup(o => o.GetAll(new Usuario()));
            var cookMock = new Mock<IAuthService>();
            var MarcarTerminadoBibliotecaController = new BibliotecaController(bibliMoq.Object, cookMock.Object, userMock.Object);
            var guardarCom = MarcarTerminadoBibliotecaController.MarcarComoTerminado(3);
            Assert.IsInstanceOf<RedirectToActionResult>(guardarCom);
        }

    }
}
